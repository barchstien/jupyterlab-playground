```bash
sudo apt install libpq-dev
# npm from repo 20.04 is too old
curl -fsSL https://deb.nodesource.com/setup_14.x | sudo -E bash -
sudo apt-get install -y nodejs

python3 -m venv env
pip install wheel
pip install -r requirements.txt

jupyter labextension list
jupyter labextension install jupyterlab-plotly

jupyter-lab
```
